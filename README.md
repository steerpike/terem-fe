# Terem Frontend Developer Test

This is an example repository for implementing the [designs from the Terem front end test](https://drive.google.com/file/d/1sgR470PR1TljP6bYQmCP0SCMIH-cm7fN/view)

## Running the example
Clone this repo and run the following
```
yarn install
yarn start
```

## Running tests
```
yarn run cypress open
```
When cypress loads, run the required spec in the interface.

## Example site
An example of the site can be found at:
https://steerpike.gitlab.io/terem-fe/

### Deploying
The above example site builds automatically when changes are pushed to Master

