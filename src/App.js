import React, { useState, useEffect } from 'react';
import PopularList from './components/PopularList'
import FeaturedList from './components/FeaturedList'

import './App.css';

function App() {
  const [searchText, setSearchText] = useState("");
  const [popular, setPopular] = useState([])
  const [featured, setFeatured] = useState([])
  async function fetchPopular() {
    const res = await fetch("https://demo3136867.mockable.io/carousel");
    res
      .json()
      .then(res => setPopular(res.data))
      .catch(err => console.log(err));
  }
  async function fetchFeatured() {
    const res = await fetch("https://demo3136867.mockable.io/featured");
    res
      .json()
      .then(res => setFeatured(res.data))
      .catch(err => console.log(err));
  }
  useEffect(() => {
    fetchPopular();
    fetchFeatured();
  }, []);

  const searchedItems = popular.filter(
    item =>
      item.title.toLocaleLowerCase().includes(searchText)
  );
  const popularToDisplay = searchText ? searchedItems : popular;
  return (
    <div className="app">
        <header>
          <input type="text" 
            name="search"
            data-test="search-field"
            placeholder="Search for..."
            value={searchText}
            onChange={e => setSearchText(e.target.value.toLocaleLowerCase())} />
        </header>
        <main>
          <PopularList popular={popularToDisplay} />
          <FeaturedList featured={featured} />
        </main>
    </div>
  );
}

export default App;
