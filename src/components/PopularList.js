import React, { useState } from 'react';
import PopularItem from './PopularItem'
import ItemsCarousel from 'react-items-carousel';

const PopularList = ({ popular }) => {
    const [activeItemIndex, setActiveItemIndex] = useState(0);
    const chevronWidth = 40;
    return (
        <div>
            <h2>Popular around you</h2>
            <ItemsCarousel
                requestToChangeActive={setActiveItemIndex}
                activeItemIndex={activeItemIndex}
                numberOfCards={4}
                gutter={20}
                leftChevron={<button className="chevron-left">{'<'}</button>}
                rightChevron={<button className="chevron-right">{'>'}</button>}
                outsideChevron
                chevronWidth={chevronWidth}
                classes={{itemWrapper:'popular-wrap'}}
           >
            {popular.map((item, id) => {
                return <PopularItem key={id} item={item} />;
            })}
            </ItemsCarousel> 
        </div>
    );
};

export default PopularList;