import React from 'react';

const FeaturedItem = ({ item }) => {
    return (
        <aside data-test="featured-item">
            <img src={item.img} width="170px" height="170px" alt={item.title}/>
            <div>
                <h3>{item.title}</h3>
                <p>
                <img src="/terem-fe/img/location_pin.png" width="24px" role="presentation" alt=""/>
                {item.location}</p>
            </div>
        </aside>
    );
};

export default FeaturedItem;