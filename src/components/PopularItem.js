import React from 'react';

const PopularItem = ({ item }) => {
    return (
        <aside data-test="popular-item">
            <img src={item.img} width="265px" height="120px" alt={item.title} />
            <div>
            <h3>{item.title}</h3>
                <p>
                <img src="/terem-fe/img/location_pin.png" width="24px" role="presentation" alt="" />
                {item.location}</p>
            </div>
        </aside>
    );
};

export default PopularItem;