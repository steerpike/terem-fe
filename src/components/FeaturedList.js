import React from 'react';
import FeaturedItem from './FeaturedItem'

const FeaturedList = ({ featured }) => {
    return (
        <div>
            <h2>Featured</h2>
            <section data-test="featured-list">
                {featured.map((item, id) => {
                    return <FeaturedItem key={id} item={item} />
                })}
            </section>
        </div>
    );
};

export default FeaturedList;