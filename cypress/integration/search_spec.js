describe('Tests page load and search filtering', () => {
    it('displays the homepage ', () => {
        cy.visit("http://localhost:3000");
        cy.get("h2").should(($h2) => {
            expect($h2).to.contain('Popular around you') 
        })
        cy.get('[data-test="search-field"]').type('ul')
        cy.get('[data-test="popular-item"]').should('have.length', 2)
    })
})
